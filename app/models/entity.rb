class Entity < ActiveRecord::Base
	belongs_to :project

	validates :name, presence: true

	default_scope { order('step_id','updated_at') }

	def to_param
    	"#{id} #{name}".parameterize
  	end

	#include PublicActivity::Model
  	#tracked owner: Proc.new{ |controller, model| controller.current_user }
end