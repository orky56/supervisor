class Task < ActiveRecord::Base
  belongs_to :project

  validates :description, presence: true

  default_scope { order('importance DESC', 'priority DESC', 'duedate','updated_at') }

  def to_param
    "#{id} #{description}".parameterize
  end

end