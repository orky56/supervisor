class Step < ActiveRecord::Base
  belongs_to :project
  has_many :entities

  validates :description, presence: true

  default_scope { order('created_at') }

  def to_param
    "#{id} #{description}".parameterize
  end

  #include PublicActivity::Model
  #tracked owner: Proc.new{ |controller, model| controller.current_user }
end