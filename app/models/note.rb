class Note < ActiveRecord::Base
	belongs_to :account

	validates :content, presence: true

	default_scope { order('ltdaccess','updated_at') }

	#include PublicActivity::Model
 	#tracked owner: Proc.new{ |controller, model| controller.current_user }
end
