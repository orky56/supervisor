class Account < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :users, :inverse_of => :account, :dependent => :destroy  
  accepts_nested_attributes_for :users
  has_many :projects
  has_many :notes

end
