class Project < ActiveRecord::Base
  has_many :tasks
  	accepts_nested_attributes_for :tasks, :reject_if => :all_blank, :allow_destroy => true
  	has_many :steps
  	accepts_nested_attributes_for :steps, :reject_if => :all_blank, :allow_destroy => true
  	has_many :entities
  	accepts_nested_attributes_for :entities, :reject_if => :all_blank, :allow_destroy => true
  	
	belongs_to :account

	validates :title, presence: true

  default_scope { order('updated_at') }

  def to_param
    "#{id} #{title}".parameterize
  end

  #include PublicActivity::Model
  #tracked owner: Proc.new{ |controller, model| controller.current_user }
end
