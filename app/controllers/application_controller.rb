class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  @projects = Project.all
  @users = User.all
  @accounts = Account.all
  @notes = Note.all

  include PublicActivity::StoreController

end
