class StepsController < ApplicationController
  before_action :set_step, only: [:show, :edit, :update, :destroy]

  # GET /steps
  # GET /steps.json
  def index
    @steps = Step.all
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
  end

  # GET /steps/1
  # GET /steps/1.json
  def show
  end

  # GET /steps/new
  def new
    @project = Project.find(params[:project_id])
    @step = @project.steps.build(:order => @project.steps.count)
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
  end

  # GET /steps/1/edit
  def edit
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
  end

  # POST /steps
  # POST /steps.json
  def create
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @project = Project.find(params[:project_id])
    @step = @project.steps.new(step_params)

    respond_to do |format|
      if @step.save
        format.html { redirect_to @project, notice: 'Step was successfully created.' }
        format.json { render :show, status: :created, location: @step }
      else
        format.html { render :new }
        format.json { render json: @step.errors, status: :unprocessable_entity }
      end
    end
  end

  #def order_up
   # @users = User.all
   # @project = Project.find(3)
   # @soriginalorder = 3
   # @scounter = 0

   # @project.steps.each do |step|
   #  @scounter = @scounter + 1
   #   if @scounter == @soriginalorder-1
   #     step.order = @scounter + 1
   #   end

   #   if @scounter == @soriginalorder
   #     step.order = @scounter-1
   #   end
   # end
  #end

  # PATCH/PUT /steps/1
  # PATCH/PUT /steps/1.json
  def update
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @project = Project.find(@step.project_id)
    respond_to do |format|
      if @step.update(step_params)
        format.html { redirect_to @project, notice: 'Step was successfully updated.' }
        format.json { render :show, status: :ok, location: @step }
      else
        format.html { render :edit }
        format.json { render json: @step.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /steps/1
  # DELETE /steps/1.json
  def destroy
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @project = Project.find(@step.project_id)
    @step.destroy
    respond_to do |format|
      format.html { redirect_to @project, notice: 'Step was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_step
      @step = Step.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def step_params
      params.require(:step).permit(:id, :description, :done, :order, :assignee, :project_id, :created_at, :updated_at)
    end
end
