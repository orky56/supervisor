class UsersController < ApplicationController
	before_filter (:authenticate_user! || :authenticate_account!)
	def new     
    	@user = current_account.users.build
    	@users = User.where('account_num = ?',current_user.account_num)
		@projects = Project.where("creator IN (?)", @users.pluck(:email))
	end    

	def create         
	  @users = User.where('account_num = ?',current_user.account_num)
	  @projects = Project.where("creator IN (?)", @users.pluck(:email))
	  @user.skip_confirmation! # confirm immediately--don't require email confirmation     
	  if @user.save       
	    flash[:success] = "User added and activated."       
	    redirect_to users_path # list of all users     
	  else       
	    render 'new'     
	  end   
	end

	def index
		@users = User.where('account_num = ?',current_user.account_num)
		@projects = Project.where("creator IN (?)", @users.pluck(:email))
		if current_user.supervisor
			@users = User.all
			@projects = Project.all
			@notes = Note.all
			@tasks = Task.all
			@steps = Step.all
			@entities = Entity.all
		end
	end

	private

	# Use strong_parameters for attribute whitelisting
	# Be sure to update your create() and update() controller methods.

end