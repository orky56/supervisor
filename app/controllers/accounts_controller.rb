class AccountsController < ApplicationController
  def new     
    @accounts = Account.new     
    @accounts.users.build  
  end    

  def create     
    @account = Account.new(params[:account])     
    if @account.save       
      flash[:success] = "Account created"       
      redirect_to accounts_path     
    else       
      render 'new'     
    end   
  end

  def index
    @users = User.all
    #@users = User.where("account_id = ?", current_account.id)
  end


end
