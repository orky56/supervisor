class PagesController < ApplicationController
  def home
  end

  def about
  end
  
  def settings
  end

  def dashboard
  	@users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @tasks = Task.where("project_id IN (?)", @projects.pluck(:id))
    @notes = Note.where("creator IN (?)", @users.pluck(:email))
    @steps = Step.where("project_id IN (?)", @projects.pluck(:id))
    @entities = Entity.where("project_id IN (?)", @projects.pluck(:id))
    if current_user.supervisor
      @users = User.all
      @projects = Project.all
      @notes = Note.all
      @tasks = Task.all
      @steps = Step.all
      @entities = Entity.all
    end
  end

  def activity
    @activities = PublicActivity::Activity.all
  end

end
