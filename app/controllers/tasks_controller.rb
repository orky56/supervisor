class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy]

  # GET /tasks
  # GET /tasks.json
  def index
    @users = User.where('account_num = ?',current_user.account_num)
    @tasks = Task.where("creator IN (?)", @users.pluck(:email))
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
  end

  # GET /tasks/new
  def new
    @users = User.where('account_num = ?',current_user.account_num)
    @project = Project.find(params[:project_id])
    @task = @project.tasks.build
  end

  # GET /tasks/1/edit
  def edit
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @project = Project.find(params[:project_id])
    @task = @project.tasks.new(task_params)

    respond_to do |format|
      if @task.save
        format.html { redirect_to @project, notice: 'Task was successfully created.' }
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @project = Project.find(@task.project_id)


    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to @project, notice: 'Task was successfully updated.' }
        format.json { render :show, status: :ok, location: @task }
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  def pause
    @users = User.where('account_num = ?',current_user.account_num)
    @project = Project.find(@task.project_id)
    @task.paused_at = DateTime.now
    @task.save

    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to @project, notice: 'Task was successfully paused.' }
        format.json { render :show, status: :ok, location: @task }
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @project = Project.find(@task.project_id)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @task.destroy
    respond_to do |format|
      format.html { redirect_to @project, notice: 'Task was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def stop
    @task = Task.find(params[:id])
    @task.started_at = nil
    @task.save!

    # Do something
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:id, :description, :done, :priority, :started, :completed_at, :time_limit, :assignee, :creator, :accepted, :project_id, :started_at, :paused_at, :resumed_at, :time_spent, :due, :duedate, :duetime, :repeat, :importance)
    end
end
