class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]

  # GET /projects
  # GET /projects.json
  def index
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @tasks = Task.where("project_id IN (?)", @projects.pluck(:id))
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @users = User.where('account_num = ?',current_user.account_num)
    @notes = Note.where("creator IN (?)", @users.pluck(:email))
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @tasks = Task.where("project_id IN (?)", @projects.pluck(:id))
  end

  # GET /projects/new
  def new
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @project = Project.new
  end

  # GET /projects/newchecklist
  def newchecklist
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @project = Project.new
  end

  # GET /projects/newprocess
  def newprocess
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
  end

  # POST /projects
  # POST /projects.json
  def create
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
        format.js
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
        format.js
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully deleted.' }
      format.json { head :no_content }
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:title, :category, :description, :creator, entities_attributes: [:id, :name, :notes, :contact, :step_id, :project_id, :created_at, :updated_at], tasks_attributes: [:id, :description, :done, :priority, :assignee, :started, :started_at, :paused_at, :completed_at, :creator, :created_at, :accepted, :completed_at, :_destroy])
    end
end
