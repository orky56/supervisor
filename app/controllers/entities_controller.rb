class EntitiesController < ApplicationController
  before_action :set_entity, only: [:show, :edit, :update, :destroy]

  # GET /entities
  # GET /entities.json
  def index
    @entities = Entity.all
  end

  # GET /entities/1
  # GET /entities/1.json
  def show
  end

  # GET /entities/new
  def new
    @project = Project.find(params[:project_id])
    @entity = @project.entities.build
  end

  # GET /entities/1/edit
  def edit
    @users = User.where('account_num = ?',current_user.account_num)
    @projects = Project.where("creator IN (?)", @users.pluck(:email))
  end

  # POST /entities
  # POST /entities.json
  def create
    @users = User.where('account_num = ?',current_user.account_num)
    @project = Project.find(params[:project_id])
    @entity = @project.entities.new(entity_params)

    respond_to do |format|
      if @entity.save
        format.html { redirect_to @project, notice: 'Entity was successfully created.' }
        format.json { render :show, status: :created, location: @entity }
      else
        format.html { render :new }
        format.json { render json: @entity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /entities/1
  # PATCH/PUT /entities/1.json
  def update
    @users = User.where('account_num = ?',current_user.account_num)
    @project = Project.find(@entity.project_id)
    respond_to do |format|
      if @entity.update(entity_params)
        format.html { redirect_to @project, notice: 'Entity was successfully updated.' }
        format.json { render :show, status: :ok, location: @entity }
      else
        format.html { render :edit }
        format.json { render json: @entity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entities/1
  # DELETE /entities/1.json
  def destroy
    @project = Project.find(@entity.project_id)
    @entity.destroy
    respond_to do |format|
      format.html { redirect_to @project, notice: 'Entity was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entity
      @entity = Entity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entity_params
      params.require(:entity).permit(:id, :name, :notes, :contact, :step_id, :project_id, :created_at, :updated_at)
    end
end
