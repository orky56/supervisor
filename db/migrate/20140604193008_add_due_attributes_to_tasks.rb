class AddDueAttributesToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :duedate, :date
    add_column :tasks, :duetime, :time
  end
end
