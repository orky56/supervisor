class AddTaskTimestampsToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :started_at, :datetime
    add_column :tasks, :completed_at, :datetime
    add_column :tasks, :paused_at, :datetime
    add_column :tasks, :resumed_at, :datetime
    add_column :tasks, :time_spent, :integer
  end
end
