class AddProjectToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :project, :string
  end
end
