class AddEntityDetailsToEntities < ActiveRecord::Migration
  def change
    add_column :entities, :name, :string
    add_column :entities, :notes, :string
    add_column :entities, :contact, :string
    add_column :entities, :project_id, :integer
  end
end
