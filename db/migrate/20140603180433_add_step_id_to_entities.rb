class AddStepIdToEntities < ActiveRecord::Migration
  def change
    add_column :entities, :step_id, :integer
  end
end
