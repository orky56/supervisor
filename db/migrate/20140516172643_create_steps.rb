class CreateSteps < ActiveRecord::Migration
  def change
    create_table :steps do |t|
      t.string :description
      t.integer :order
      t.boolean :done
      t.belongs_to :project, index: true

      t.timestamps
    end
  end
end
