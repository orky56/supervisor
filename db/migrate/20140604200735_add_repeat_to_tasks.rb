class AddRepeatToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :repeat_time, :time
    add_column :tasks, :repeat_day, :string
    add_column :tasks, :repeat_date, :integer
    add_column :tasks, :repeat_ordinal, :integer
  end
end
