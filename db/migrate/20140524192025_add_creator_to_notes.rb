class AddCreatorToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :creator, :string
  end
end
