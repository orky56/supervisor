class RenameCompaniesToAccounts < ActiveRecord::Migration
  def change
  	rename_table :companies, :accounts
  end
end