class AddPrivacyToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :private, :boolean
  end
end
