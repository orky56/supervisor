class AddStatusesToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :started, :boolean
    add_column :tasks, :active, :boolean
  end
end
