class AddImportancesToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :repeat, :string
    add_column :tasks, :time_limit, :integer
    add_column :tasks, :priority, :string
  end
end
